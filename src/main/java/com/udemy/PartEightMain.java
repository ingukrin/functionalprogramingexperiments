package com.udemy;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PartEightMain {
    public static void main(String[] args) {
        List<String> listWithString = List.of("Spring", "Spring Boot", "API","Microservices", "AWS", "PCF", "Azure", "Docker", "Kubernetes");

        List<String> modufyableCourses = new ArrayList(listWithString);

        System.out.println(modufyableCourses);

        //experimentsWitheRemoveIfAndReplaceALl(modufyableCourses);

//        try {
//            ExperimentsWithFiles();
//        }
//        catch (Exception e){
//            System.out.println("Exsption Appears");
//        }

        ///Treads
        Runnable rinnable = new Runnable() {
            @Override
            public void run() {
                
            }
        }



    }

    private static void ExperimentsWithFiles() throws IOException {
        //Files.lines(Paths.get("File.txt")).forEach(System.out::println);
//        Files.lines(Paths.get("C:\\Users\\ingus\\IdeaProjects\\FuncionalProgramngUdemy\\src\\main\\resources\\File.txt")).forEach(System.out::println);


        experimentsWithFiles();

    }

    private static void experimentsWithFiles() throws IOException {
        Files.lines(Paths.get("C:\\Users\\ingus\\IdeaProjects\\FuncionalProgramngUdemy\\src\\main\\resources\\File.txt"))
                //.map(str->str.split(""))
                .map(str->str.split(" "))
                .flatMap(Arrays::stream)
                .distinct()
                .sorted()
                .forEach(System.out::println);

//        System.out.println(
//                Files.lines(Paths.get("."))
//                        .filter(Files::isReadable)
//                        .forEach(System.out::println)
//                );
    }

    private static void experimentsWitheRemoveIfAndReplaceALl(List<String> modufyableCourses) {
        ///Replace All
        modufyableCourses.
                replaceAll(str->str.toUpperCase());
        System.out.println(modufyableCourses);

        ///Remove if
        modufyableCourses.
                removeIf(str->str.length()<6);
        System.out.println(modufyableCourses);
    }
}
