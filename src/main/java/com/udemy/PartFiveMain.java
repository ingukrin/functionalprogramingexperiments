package com.udemy;

import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

class Course{
    private String name;
    private String category;
    private int reviewScore;
    private int noOfStudents;

    public Course(String name, String category, int reviewScore, int noOfStudents) {
        this.name = name;
        this.category = category;
        this.reviewScore = reviewScore;
        this.noOfStudents = noOfStudents;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategoryl() {
        return category;
    }

    public void setCategoryl(String categoryl) {
        this.category = categoryl;
    }

    public int getReviewScore() {
        return reviewScore;
    }

    public void setReviewScore(int reviewScore) {
        this.reviewScore = reviewScore;
    }

    public int getNoOfStudents() {
        return noOfStudents;
    }

    public void setNoOfStudents(int noOfStudents) {
        this.noOfStudents = noOfStudents;
    }

    @Override
    public String toString() {
        return  name + ":" +
                category + ":" +
                reviewScore + ":"+
                noOfStudents ;
    }
}

public class PartFiveMain {


    public static void main(String[] args) {
List<Course> coursesList =  List.of(
                new Course("Spring","Framework", 98, 20000),
                new Course("Spring Boot","Framework", 95, 18000),
                new Course("API","Microservices", 97, 22000),
                new Course("Microservices","Microservices", 96, 25000),
                new Course("FullStack","FullStack", 91, 14000),
                new Course("AWS","Cloud", 92, 21000),
                new Course("Azure","Cloud", 99, 21000),
                new Course("Docker","Cloud", 92, 20000),
                new Course("Kubernates","Cloud", 91, 20000)
        );
        //experimentsWithmachCase(coursesList);
        //experimentsByComperaTor(coursesList);
        //experionetsWithSkipLimitTakeWhileDropWhile(coursesList);
        //playingWithStreemsAgain(coursesList);
        //experiomentsWithPrimitiveValues(coursesList);
        //experimentWithGrouping(coursesList);

    }

    private static void experimentWithGrouping(List<Course> coursesList) {
        //Group corses by category
        System.out.println(
        coursesList.stream()
                .collect(Collectors.groupingBy(Course::getCategoryl))
        );
        //{v=[AWS:v:92:21000], Cloud=[Azure:Cloud:99:21000, Docker:Cloud:92:20000, Kubernates:Cloud:91:20000],
        // FullStack=[FullStack:FullStack:91:14000],
        // Microservices=[API:Microservices:97:22000, Microservices:Microservices:96:25000],
        // Framework=[Spring:Framework:98:20000, Spring Boot:Framework:95:18000]}

        System.out.println(
                coursesList.stream()
                        .collect(Collectors
                        .groupingBy(Course::getCategoryl,Collectors.counting()))
        );
        //{Cloud=4, FullStack=1, Microservices=2, Framework=2}

        System.out.println(
                coursesList.stream()
                        .collect(Collectors.groupingBy(Course::getCategoryl,Collectors.maxBy(Comparator.comparing(Course::getReviewScore))))
        );
        //{Cloud=Optional[Azure:Cloud:99:21000],
        // FullStack=Optional[FullStack:FullStack:91:14000],
        // Microservices=Optional[API:Microservices:97:22000],
        // Framework=Optional[Spring:Framework:98:20000]}

        System.out.println(
                coursesList.stream()
                        .collect(Collectors.groupingBy(Course::getCategoryl,
                                 Collectors.mapping(Course::getName,Collectors.toList())))
        );
        //{Cloud=[AWS, Azure, Docker, Kubernates], FullStack=[FullStack], Microservices=[API, Microservices], Framework=[Spring, Spring Boot]}
    }

    private static void experiomentsWithPrimitiveValues(List<Course> coursesList) {
        Predicate<Course> revieScoreGreterThan95Predicate
                =course -> course.getReviewScore()>95;
        System.out.println(coursesList.stream()
                .filter(revieScoreGreterThan95Predicate)
                .mapToInt(Course::getNoOfStudents)
                .sum());

        System.out.println(coursesList.stream()
                .filter(revieScoreGreterThan95Predicate)
                .mapToInt(Course::getNoOfStudents)
                .average());

        System.out.println(coursesList.stream()
                .filter(revieScoreGreterThan95Predicate)
                .count());

        System.out.println(coursesList.stream()
                .filter(revieScoreGreterThan95Predicate)
                .mapToInt(Course::getNoOfStudents)
                .max());

        System.out.println(coursesList.stream()
                .filter(revieScoreGreterThan95Predicate)
                .mapToInt(Course::getNoOfStudents)
                .min());
    }

    private static void playingWithStreemsAgain(List<Course> coursesList) {
        Comparator<Course> comparingByNoOfStudentsDecreasingAndNumberOfReview
                = Comparator.comparing(Course::getNoOfStudents)
                .thenComparing(Course::getReviewScore);
        ///Max foundmax valu by comperator
        coursesList.stream().max(comparingByNoOfStudentsDecreasingAndNumberOfReview).stream().forEach(System.out::println);
        //Microservices:Microservices:96:25000
        System.out.println(coursesList.stream().max(comparingByNoOfStudentsDecreasingAndNumberOfReview));
        //Optional[Microservices:Microservices:96:25000]
        System.out.println(coursesList.stream().min(comparingByNoOfStudentsDecreasingAndNumberOfReview));
        //Optional[FullStack:FullStack:91:14000]

        Predicate<Course> revieScoreLessThan90Predicate
                =course -> course.getReviewScore()<90;
        Predicate<Course> revieScoreGreterThan95Predicate
                =course -> course.getReviewScore()>95;


        System.out.println(coursesList.stream()
                .filter(revieScoreLessThan90Predicate)
                .min(comparingByNoOfStudentsDecreasingAndNumberOfReview));
        //Optional.empty
        System.out.println(coursesList.stream()
                .filter(revieScoreLessThan90Predicate)
                .min(comparingByNoOfStudentsDecreasingAndNumberOfReview)
                ///If no any value returned, then it can be returned by orElse
                .orElse( new Course("Kubernates","Cloud", 91, 20000)));
        //Else value--> Kubernates:Cloud:91:20000

        System.out.println(coursesList.stream()
                .filter(revieScoreLessThan90Predicate)
                .min(comparingByNoOfStudentsDecreasingAndNumberOfReview)
                ///If no any value returned, then it can be returned by orElse
                .orElse( new Course("Kubernates","Cloud", 91, 20000)));
        //Else value--> Kubernates:Cloud:91:20000


        System.out.println(coursesList.stream()
                .filter(revieScoreGreterThan95Predicate)
                .findFirst()); ///Find First element
        //Optional[Spring:Framework:98:20000]
        System.out.println(coursesList.stream()
                .filter(revieScoreGreterThan95Predicate)
                .findAny());///Find Any element
        //Optional[Spring:Framework:98:20000]
    }

    private static void experionetsWithSkipLimitTakeWhileDropWhile(List<Course> coursesList) {
        Comparator<Course> comparingByNoOfStudentsIncreasing
                = Comparator.comparing(Course::getNoOfStudents);
        // Limit number of Value on outpot
        System.out.println(coursesList.stream()
                .sorted(comparingByNoOfStudentsIncreasing)
                .limit(5)
                .collect(Collectors.toList()));

        // Skip first 3 output values and get after that -->
        System.out.println(coursesList.stream()
                    .sorted(comparingByNoOfStudentsIncreasing)
                    .skip(3)
                    .collect(Collectors.toList()));

        ///Get values till one element Do not mach predicate conditions--> takeWhile
        List<Course> course1 = coursesList.stream()
                .takeWhile(cour -> cour.getReviewScore()>=93)
                .collect(Collectors.toList());
        System.out.println(course1);


        ///drop all values till one element Do not mach predicate conditions--> dropWhile
        List<Course> course2 = coursesList.stream()
                .dropWhile(cour -> cour.getReviewScore()>=93)
                .collect(Collectors.toList());
        System.out.println(course2);
    }

    private static void experimentsByComperaTor(List<Course> coursesList) {
        Comparator<Course> comparingByNoOfStudentsIncreasing
                = Comparator.comparing(Course::getNoOfStudents);
        Comparator<Course> comparingByNoOfStudentsDecreasing
                = Comparator.comparing(Course::getNoOfStudents).reversed();
        System.out.println(coursesList.stream().sorted(comparingByNoOfStudentsDecreasing).collect(Collectors.toList()));

        Comparator<Course> comparingByNoOfStudentsDecreasingAndNumberOfReview
                        = Comparator.comparing(Course::getNoOfStudents)
                         .thenComparing(Course::getReviewScore);
        System.out.println(coursesList.stream().sorted(comparingByNoOfStudentsDecreasingAndNumberOfReview).collect(Collectors.toList()));
    }


    private static void experimentsWithmachCase(List<Course> coursesList) {
        Predicate<Course> revieScoreGreaterThan95Predicate
                =course -> course.getReviewScore()>95;

        Predicate<Course> revieScoreGreaterThan90Predicate
                =course -> course.getReviewScore()>90;

        Predicate<Course> revieScoreLessThan90Predicate
                =course -> course.getReviewScore()<90;

        //True is all in list mach to predicate condition, else false
        System.out.println( coursesList.stream().allMatch(revieScoreGreaterThan95Predicate));
        //True is non value in list mach to predicate condition, else false
        System.out.println( coursesList.stream().noneMatch(revieScoreLessThan90Predicate));
        //True is anh value in list mach to predicate condition, else false
        System.out.println( coursesList.stream().anyMatch(revieScoreGreaterThan90Predicate));

        //        Try by ourself

        Predicate<Course> noOfStudentsThan1000Predicate
                =course -> course.getNoOfStudents()>10000;

        Predicate<Course> noOfStudentsLessThan150000Predicate
                =course -> course.getNoOfStudents()<15000;

        Predicate<Course> noOfStudentLessThan20000Predicate
                =course -> course.getNoOfStudents()<20000;

        System.out.println(coursesList.stream().anyMatch(noOfStudentLessThan20000Predicate));
        System.out.println(coursesList.stream().noneMatch(noOfStudentsLessThan150000Predicate));
        System.out.println(coursesList.stream().allMatch(noOfStudentsThan1000Predicate));
    }

}
