package com.udemy;

import java.util.List;
import java.util.stream.Stream;

public class Main {


    public static void main(String[] args) {
//        List.of(2,9,13,4,6,2,4,12,15).stream().forEach(System.out::println);
        List<Integer> listWithNumbers = List.of(2, 9, 13, 4, 6, 2, 4, 12, 15);
        List<String> listWithString = List.of("Spring", "Spring Boot", "API","Microservices", "AWS", "PCF", "Azure", "Docker", "Kubernetes");
        //PrintAllNumbers(listWithNumbers);
        //PrintEvenNumbers(listWithNumbers);
        //printOutContainsSpring(listWithString);
        //printOutStringLongerThanThree(listWithString);
        //printSoutCourseNameWithLenghtByUsingMap(listWithString);
        //oddNumberInCube(listWithNumbers);


    }

    private static void oddNumberInCube(List<Integer> listWithNumbers) {
        listWithNumbers.stream().filter(a->a%2!=0).map(a->a*a*a).forEach(System.out::println);
    }

    private static void printSoutCourseNameWithLenghtByUsingMap(List<String> listWithString) {
        listWithString.stream().map(courses->courses+" "+courses.length()).forEach(System.out::println);
    }

    private static void printOutStringLongerThanThree(List<String> listWithString) {
        listWithString.stream().filter(course ->course.length()>=4).forEach(System.out::println);
    }

    private static void printOutContainsSpring(List<String> listWithString) {
        //Filter Spring from list
        listWithString.stream()
                .filter(textSpring->textSpring.contains("Spring"))
                .forEach(System.out::println);
    }

    private static void PrintEvenNumbers(List listWithNumbers) {
        //Drukā pāra numurus
        listWithNumbers.stream()
                .filter(number -> number.hashCode()%2 == 0)
                .forEach(System.out::println);
    }

    private static void PrintAllNumbers(List<Integer> listWithNumbers) {
        // Print out list with forEach
        listWithNumbers.stream()
                .forEach(System.out::println);
    }
}
