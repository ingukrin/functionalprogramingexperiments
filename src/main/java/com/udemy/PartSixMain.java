package com.udemy;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class PartSixMain {
    public static void main(String[] args) {
        System.out.println(Stream.of(12,9,13,4,6,2,4,12,15).count());
        System.out.println(Stream.of(12,9,13,4,6,2,4,12,15).reduce(0,Integer::sum));
        int[] numberArray ={12,9,13,4,6,2,4,12,15};
        System.out.println(Arrays.stream(numberArray).max());

        System.out.println(IntStream.range(1,10).peek(System.out::println).sum());
        System.out.println(IntStream.rangeClosed(1,10).peek(System.out::println).sum());
        System.out.println(IntStream.iterate(1,e->e+2).limit(10).peek(System.out::println).sum());
        System.out.println(IntStream.iterate(2,e->e+2).limit(10).peek(System.out::println).sum());
        System.out.println(IntStream.iterate(2,e->e*2).limit(10).peek(System.out::println).sum());
        System.out.println(IntStream.iterate(2,e->e*2).limit(10).boxed().collect(Collectors.toList()));
          System.out.println(Integer.MAX_VALUE);
          System.out.println(Long.MAX_VALUE);
            System.out.println(LongStream.range(1,50).mapToObj(BigInteger::valueOf).reduce(BigInteger.ONE, BigInteger::multiply));

    }
}

