package com.udemy;

import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class PlayingWithStreems {

    public static void main(String[] args) {
        List<Integer> listWithNumbers = List.of(2, 9, 13, 4, 6, 2, 4, 12, 15);
        List<String> listWithString = List.of("Spring", "Spring Boot", "API", "Microservices", "AWS", "PCF", "Azure", "Docker", "Kubernetes");
        //System.out.println(listWithNumbers.stream().reduce(0,PlayingWithStreems::sum));
        //System.out.println(sumInFunctionalWay(listWithNumbers));
//        System.out.println(calculateSumOfIntegersInSquare(listWithNumbers));
//        System.out.println(calculateOddNumberSum(listWithNumbers));
        //printOutDistinctNumbersFromList(listWithNumbers);

        //SortingNumbers
//         printOutSortedNumberList(listWithNumbers);

        //Sorting Strings
        //listWithString.stream().sorted().forEach(System.out::println);
//        listWithString.stream()
//                .sorted(Comparator.naturalOrder())
//                .forEach(System.out::println);

//        listWithString.stream()
//                .sorted(Comparator.reverseOrder())
//                .forEach(System.out::println);

//        Collect data from Stream
//        List<Integer> listWithNumbersDoubled = listWithNumbers.stream()
//                .map(number->number*number)
//                .collect(Collectors.toList());
//        System.out.println(listWithNumbers);
//        System.out.println(listWithNumbersDoubled);

//        List<Integer> lenghOfCourseTitle = listWithString.stream()
//                .map(title->title.length())
//                .collect(Collectors.toList());
//        System.out.println(lenghOfCourseTitle);

//        List<Integer> listOfEvenNumberNumberInCube = listWithNumbers.stream()
//                .filter(even->even%2==0)
//                .map(number->number*number*number)
//                .collect(Collectors.toList());
//        System.out.println(listOfEvenNumberNumberInCube);

        //System.out.println(sumBinaryOperator(listWithNumbers));


//        List<Integer> intListInSquareNumbers= getIntListInC ube(listWithNumbers, x->x*x);
//        List<Integer> intListInCubeNumbers= getIntListInCube(listWithNumbers, x->x*x*x);
//        System.out.println(intListInSquareNumbers);
//        System.out.println(intListInCubeNumbers);
        //Supplier
//        Supplier<Integer> randomSuplier = ()->{
//            Random random = new Random();
//            return random.nextInt(1000);
//        };
//
//        System.out.println(randomSuplier.get());

        //System.out.println(listWithString.stream().map(a->1).reduce(0,(a,b)->a+b));

        listWithString.stream().map(String::toUpperCase).forEach(System.out::println);

    }

    private static List<Integer> getIntListInCube(List<Integer> listWithNumbers, Function<Integer, Integer> mappingFunction) {
        return listWithNumbers.stream().map(mappingFunction).collect(Collectors.toList());
    }


    private static Integer sumBinaryOperator(List<Integer> listWithNumbers) {
        return listWithNumbers.stream().reduce(0, getSum());
    }

    private static BinaryOperator<Integer> getSum() {
        return Integer::sum;
    }

    private static void printOutSortedNumberList(List<Integer> listWithNumbers) {

        listWithNumbers.stream().sorted().forEach(System.out::println);
    }

    private static void printOutDistinctNumbersFromList(List<Integer> listWithNumbers) {

        listWithNumbers.stream().distinct().forEach(System.out::println);
    }

    private static Integer calculateOddNumberSum(List<Integer> listWithNumbers) {

        return listWithNumbers.stream().filter(a -> a % 2 > 0).reduce(0, Integer::sum);
    }

//    private static Integer calculateSumOfIntegersInSquare(List<Integer> listWithNumbers) {
//
//        return listWithNumbers.stream().map(getIntegerIntegerFunction()).reduce(0, Integer::sum);
//    }

    private static Integer sumInFunctionalWay(List<Integer> listWithNumbers) {

        //return listWithNumbers.stream().reduce(0, (a, b) -> a + b);
        return listWithNumbers.stream().reduce(0, Integer::sum);
    }

    private static Integer minInFunctionalWay(List<Integer> listWithNumbers) {

        return listWithNumbers.stream().reduce(0, (a, b) -> a < b ? a : b);
    }

    private static int sum(int agregate, int nextNumber) {

        //System.out.println("Agregate="+agregate+"; NextNumber="+nextNumber);
        return agregate + nextNumber;
    }

}
