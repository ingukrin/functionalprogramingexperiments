package com.udemy;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

public class PartSevenMain {
    public static void main(String[] args) {
        List<String> listWithString = List.of("Spring", "Spring Boot", "API","Microservices", "AWS", "PCF", "Azure", "Docker", "Kubernetes");

        //experimentsWithFlatMap(listWithString);
//
//        Predicate<Course> revieScoreGreterThan95Predicate
//                = getCoursePredicate(95);
//
//        Predicate<Course> revieScoreGreterThan90Predicate
//                =getCoursePredicate(90);

//        System.out.println(
//                listWithString.stream()
//                        //.peek(System.out::println)
//                        .filter(a->a.length()>11)
//                        //.peek(System.out::println)
//                        .map(String::toUpperCase)
//                        //.peek(System.out::println)
//                        .findFirst()
//        );
        long time = System.currentTimeMillis();
        System.out.println(
        LongStream.range(1,10000000)
                //.parallel()
                .sum()
                );
        System.out.println(System.currentTimeMillis()-time);


    }

    private static Predicate<Course> getCoursePredicate(int cutoffReviewScore) {
        return course -> course.getReviewScore() > cutoffReviewScore;
    }

    private static void experimentsWithFlatMap(List<String> listWithString) {
//        //Playing withFlatMap
        System.out.println(
                listWithString.stream()
                        .collect(Collectors.joining(" "))
        );
//        System.out.println("Spring".split(" "));
        System.out.println(
                listWithString.stream()
                        .map(course->course.split(" "))
                        .flatMap(Arrays::stream)
                        .collect(Collectors.toList())
        );

        List<String> courses = List.of("Spring", "Spring Boot", "API","Microservices", "AWS", "PCF", "Azure", "Docker", "Kubernetes");
        List<String> courses2 = List.of("Spring", "Spring Boot", "API","Microservices", "AWS", "PCF", "Azure", "Docker", "Kubernetes");

        System.out.println(
                courses.stream()
                        .flatMap(course -> courses2.stream()
                                .map(course2-> List.of(course,course2)))
                                .collect(Collectors.toList())
        );
        //[[Spring, Spring], [Spring, Spring Boot], [Spring, API], [Spring, Microservices], [Spring, AWS],
        // [Spring, PCF], [Spring, Azure], [Spring, Docker], [Spring, Kubernetes], [Spring Boot, Spring],
        // [Spring Boot, Spring Boot], [Spring Boot, API], [Spring Boot, Microservices], [Spring Boot, AWS],
        // [Spring Boot, PCF], [Spring Boot, Azure], [Spring Boot, Docker], [Spring Boot, Kubernetes], [API, Spring],
        // [API, Spring Boot], [API, API], [API, Microservices], [API, AWS], [API, PCF], [API, Azure], [API, Docker],
        // [API, Kubernetes], [Microservices, Spring], [Microservices, Spring Boot], [Microservices, API],
        // [Microservices, Microservices], [Microservices, AWS], [Microservices, PCF], [Microservices, Azure],
        // [Microservices, Docker], [Microservices, Kubernetes], [AWS, Spring], [AWS, Spring Boot], [AWS, API],
        // [AWS, Microservices], [AWS, AWS], [AWS, PCF], [AWS, Azure], [AWS, Docker], [AWS, Kubernetes], [PCF, Spring],
        // [PCF, Spring Boot], [PCF, API], [PCF, Microservices], [PCF, AWS], [PCF, PCF], [PCF, Azure], [PCF, Docker],
        // [PCF, Kubernetes], [Azure, Spring], [Azure, Spring Boot], [Azure, API], [Azure, Microservices], [Azure, AWS],
        // [Azure, PCF], [Azure, Azure], [Azure, Docker], [Azure, Kubernetes], [Docker, Spring], [Docker, Spring Boot], [Docker, API], [Docker, Microservices], [Docker, AWS], [Docker, PCF], [Docker, Azure], [Docker, Docker], [Docker, Kubernetes], [Kubernetes, Spring], [Kubernetes, Spring Boot], [Kubernetes, API], [Kubernetes, Microservices], [Kubernetes, AWS], [Kubernetes, PCF], [Kubernetes, Azure], [Kubernetes, Docker], [Kubernetes, Kubernetes]]




        System.out.println(
                courses.stream()
                        .flatMap(course -> courses2.stream()
                                .map(course2-> List.of(course,course2)))
                        .filter(list->!list.get(0).equals(list.get(1)))  /// iznem ara elemetus, kas sakrit
                        .collect(Collectors.toList())
        );
        //[[Spring, Spring Boot], [Spring, API], [Spring, Microservices], [Spring, AWS], [Spring, PCF], [Spring, Azure], [Spring, Docker], [Spring, Kubernetes], [Spring Boot, Spring], [Spring Boot, API], [Spring Boot, Microservices], [Spring Boot, AWS], [Spring Boot, PCF], [Spring Boot, Azure], [Spring Boot, Docker], [Spring Boot, Kubernetes], [API, Spring], [API, Spring Boot], [API, Microservices], [API, AWS], [API, PCF], [API, Azure], [API, Docker], [API, Kubernetes], [Microservices, Spring], [Microservices, Spring Boot], [Microservices, API], [Microservices, AWS], [Microservices, PCF], [Microservices, Azure], [Microservices, Docker], [Microservices, Kubernetes], [AWS, Spring], [AWS, Spring Boot], [AWS, API], [AWS, Microservices], [AWS, PCF], [AWS, Azure], [AWS, Docker], [AWS, Kubernetes], [PCF, Spring], [PCF, Spring Boot], [PCF, API], [PCF, Microservices], [PCF, AWS], [PCF, Azure], [PCF, Docker], [PCF, Kubernetes], [Azure, Spring], [Azure, Spring Boot], [Azure, API], [Azure, Microservices], [Azure, AWS], [Azure, PCF], [Azure, Docker], [Azure, Kubernetes], [Docker, Spring], [Docker, Spring Boot], [Docker, API], [Docker, Microservices], [Docker, AWS], [Docker, PCF], [Docker, Azure], [Docker, Kubernetes], [Kubernetes, Spring], [Kubernetes, Spring Boot], [Kubernetes, API], [Kubernetes, Microservices], [Kubernetes, AWS], [Kubernetes, PCF], [Kubernetes, Azure], [Kubernetes, Docker]]

        System.out.println(
                courses.stream()
                        .flatMap(course -> courses2.stream()
                                .map(course2-> List.of(course,course2)))
                        .filter(list->list.get(0).length()==list.get(1).length())  /// iznem ara elemetus, kas sakrit
                        .collect(Collectors.toList())
        );

    }

}
